import  MySQLdb
import pygrametl
from pygrametl.datasources import SQLSource, CSVSource
from pygrametl.tables import Dimension, FactTable
import sqlite3

#target database connection
dw_conn = MySQLdb.connect(host="localhost",    
                     user="root",         
                     passwd="qwerty",  
                     db="DW",charset="utf8")
dw_conn_wrapper = pygrametl.ConnectionWrapper(connection=dw_conn)


conn_sq = sqlite3.connect('chinook.db')
name_mapping= 'invoiceDate', 'billingAddress','name','UnitPrice', 'Quantity', 'InvoiceId','TrackId', 'Total'
sales_source = SQLSource(connection=conn_sq, \
                         query="SELECT invoices.invoiceDate, invoices.billingAddress, tracks.Name as name, invoice_items.UnitPrice,\
                          invoice_items.Quantity,invoice_items.InvoiceId,invoice_items.TrackId, invoice_items.UnitPrice*invoice_items.Quantity as Total\
						FROM invoices \
						JOIN invoice_items ON invoice_items.InvoiceId = invoices.InvoiceId \
						JOIN tracks ON invoice_items.TrackId = tracks.TrackId", names=name_mapping)




invoices = Dimension(
    name='invoices',
    key='invoiceid',
    attributes=['invoiceDate', 'billingAddress'])

tracks = Dimension(
    name='tracks',
    key='trackid',
    attributes=['name'])


fact = FactTable(
    name='track_items',
    keyrefs=['InvoiceId', 'TrackId'],
	measures=['UnitPrice','Quantity','Total']
    )

for row in sales_source:
	 invoices.insert(row)
	 tracks.insert(row)
	 fact.insert(row)

dw_conn_wrapper.commit()
dw_conn_wrapper.close()

